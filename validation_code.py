import sys
import math
def Gene_Folding(gene):
    s = gene
    def gene_valid(s):
        for u in s:
            if u.upper() not in ['A', 'C', 'G', 'T']:
                return False
        else:
            return True
